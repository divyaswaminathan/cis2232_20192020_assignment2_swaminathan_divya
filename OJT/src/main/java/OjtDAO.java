
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author divya
 */
public class OjtDAO {
    Connection conn = null;
    Statement stmt;
    /*
      Default constructor will setup the connection and statement objects to be
        used by this OjtDAO instance
    
    */
    public OjtDAO()
    {
        try
        {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ojt", "root", "");
            stmt = conn.createStatement();
        } catch(SQLException sql){
            System.out.println("sql exception caught");
            sql.printStackTrace();
        }
    }
    /*
    Show all students in the list
    */
    public ArrayList<OjtReflection> selectAll(){
        ArrayList<OjtReflection> students;
        try{
            //select all rows assign it into students object
            students = new ArrayList();
            
            ResultSet rs = stmt.executeQuery("SELECT * FROM Ojtreflection");
            while(rs.next()) {
                OjtReflection student = new OjtReflection();
                student.setStudentId(rs.getInt("studentId"));
                student.setFirstName(rs.getString(2));
                student.setLastName(rs.getString(3));
                student.setReflectionText(rs.getString(4));
                students.add(student);
                System.out.println(student.getReflectionText());
            }
            return students;
        } catch (SQLException ex)
        {
            Logger.getLogger(OjtDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }
    /*
     this method show particular student based on registration id
    */
    
    public OjtReflection select(int studentId){
        try
        {
            PreparedStatement theStatement = null;
            String sqlString = "select * from ojtreflection where studentId=?";
            theStatement = conn.prepareStatement(sqlString);
            theStatement.setInt(1, studentId);
            ResultSet rs = theStatement.executeQuery();
            OjtReflection student = null;
            
            while(rs.next()){
                student = new OjtReflection(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
            return student;
        }catch (SQLException ex) {
            Logger.getLogger(OjtDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
     /**
     * This method will delete the student associated with the student id.
     *
     * @param studentId Identifies the student to be deleted.
     * 
     * @author Divya S
     */
    public void delete(int studentId) {
        try {
            PreparedStatement deleteStatement = null;
            String sqlString = "delete from ojtreflection where studentId=?";
            deleteStatement = conn.prepareStatement(sqlString);
            deleteStatement.setInt(1, studentId);
            
            deleteStatement.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }
/**
     * This method will insert the input student. It will notify the console if
     * an exception is encountered (IE student already exists).
     *
     * @param student -> OjtReflection to insert
     * 
     * @author Divya S
     */
    public void insert(OjtReflection student) {
        try {

            
            
            PreparedStatement insertStatement = null;
            String sqlString = "INSERT INTO `ojtreflection`(`studentId`, `firstname`, `lastname`, `reflection`)"
                    + "VALUES (?,?,?,?)";
            insertStatement = conn.prepareStatement(sqlString);
            insertStatement.setInt(1, student.getStudentId());
            insertStatement.setString(2, student.getFirstName());
            insertStatement.setString(3, student.getLastName());
            insertStatement.setString(4, student.getReflectionText());

            
            
            insertStatement.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("Could not add student into ojt table");
        }

    }
     /**
     * Will update the database based on student provided.
     *
     * @param student-> ojtReflection to update
     *
     * @author Divya S
     */
    public void update(OjtReflection student) {
        try {

            String query = "UPDATE ojtreflection SET "
                    + "firstname='"+student.getFirstName()+"',lastname='"+
                    student.getLastName()+"',reflection='"+student.getReflectionText()
                    +"' WHERE studentId="+student.getStudentId();
            System.out.println("About to query:" + query);
            stmt.executeUpdate(query);

        } catch (SQLException sqle) {
            System.out.println("Could not add student");
        }
    }
    
}
