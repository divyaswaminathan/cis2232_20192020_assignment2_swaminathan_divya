 drop database ojt;
create database ojt;
use ojt;

CREATE TABLE IF NOT EXISTS OjtReflection ( 
id int(5) NOT NULL PRIMARY KEY AUTO_INCREMENT, 
studentId int(5) NOT NULL, 
name varchar(20) NOT NULL, 
reflection varchar(140) NOT NULL );