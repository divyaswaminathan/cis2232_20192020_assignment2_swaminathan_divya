package info.hccis.ojt.web;

import info.hccis.ojt.dao.OjtDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    @RequestMapping("/")
    public String showHome(Model model) {

        //Get the campers from the database
        model.addAttribute("students", OjtDAO.selectAll());
        
        //This will send the user to the welcome.html page.
        return "ojt/list";
    }

}
