

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author bjmaclean
 */
public class MainClass {

    public static String MENU = "Options:\nA) Add student\nU) Update student reflection text\nD) Delete student\nS) Show student\nX) Exit";
    public static final String PATH = "/cis2232_20192020/";
    public static String FILE_NAME = PATH + "reflection.json";

    public static void main(String[] args) throws IOException {
        ArrayList<OjtReflection> theList = new ArrayList();
        OjtDAO ojtDAO = new OjtDAO();
        String option;
        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    //System.out.println("Picked A");
                    OjtReflection newstudent = new OjtReflection(true);
                    ojtDAO.insert(newstudent);
                    break;
                case "U":
                    //System.out.println("Picked A");
                    System.out.println("Enter student id");
                    int studentIdForUpdate = FileUtility.getInput().nextInt();
                    FileUtility.getInput().nextLine(); //burn
                    OjtReflection student = ojtDAO.select(studentIdForUpdate);
                    if(student == null){
                        System.out.println("student not found");
                    }else{
                        student.updateReflection();
                        ojtDAO.update(student);
                        System.out.println("student updated");
                    }
                    break;
                case "D":
                    //System.out.println("Picked A");
                    System.out.println("Enter student id");
                    int registrationId = FileUtility.getInput().nextInt();
                    FileUtility.getInput().nextLine(); //burn
                    ojtDAO.delete(registrationId);
                    break;
                case "S":
                    System.out.println("Here are the campers");
                    theList = ojtDAO.selectAll();
                    for (OjtReflection student2 : theList) {
                        System.out.println(student2);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }

        } while (!option.equalsIgnoreCase(
                "x"));
    }
}
