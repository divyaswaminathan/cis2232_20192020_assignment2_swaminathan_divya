drop database ojt;
create database ojt;
use ojt;

/*create a user in database*/
grant select, insert, update, delete on ojt.*
to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;


CREATE TABLE IF NOT EXISTS OjtReflection (
	studentId int(5) NOT NULL,
	firstname varchar(20) NOT NULL,
        lastname varchar(20) NOT NULL,
	reflection varchar(140) NOT NULL 
	);	


