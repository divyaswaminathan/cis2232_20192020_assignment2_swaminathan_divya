package info.hccis.ojt.dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import info.hccis.ojt.entity.OjtReflection;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author divya
 */
public class OjtDAO {
    private final static Logger LOGGER = Logger.getLogger(OjtDAO.class.getName());
    
    /*
      Default constructor will setup the connection and statement objects to be
        used by this OjtDAO instance
    
    */
    
    /*
    Show all students in the list
    */
    /*public static ArrayList<OjtReflection> selectAll() {
        ArrayList<OjtReflection> students;
        Connection conn = null;
        Statement stmt = null;
        try{
            conn = ConnectionUtils.getConnection();
            //select all rows assign it into students object
            students = new ArrayList();
            
            ResultSet rs = stmt.executeQuery("SELECT * FROM Ojtreflection");
            while(rs.next()) {
                OjtReflection student = new OjtReflection();
                 
                student.setId(rs.getInt(1));
                student.setStudentId(rs.getInt("studentId"));
                student.setName(rs.getString(3));
                student.setReflection(rs.getString(4));
                students.add(student);
                System.out.println(student.getReflection());
            }
            return students;
        } catch (SQLException ex)
        {
            Logger.getLogger(OjtDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
         finally {
            DbUtils.close(stmt, conn);
        }
        
    }*/
     public static ArrayList<OjtReflection> selectAll() {

        ArrayList<OjtReflection> students = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM ojtreflection order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                int studentId = rs.getInt("studentId");
                String name = rs.getString("name");
                String reflection = rs.getString("reflection");
                OjtReflection student = new OjtReflection(id, studentId, name,reflection);
                
                students.add(student);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return students;
    }
    /*
     this method show particular student based on registration id
    */
    
   /* public static synchronized OjtReflection select(int idnum) throws Exception{
        Connection conn = null;
        Statement stmt = null;
        try
        {
            conn = ConnectionUtils.getConnection();
            PreparedStatement theStatement = null;
            String sqlString = "select * from ojtreflection where id=?";
            theStatement = conn.prepareStatement(sqlString);
            theStatement.setInt(1, idnum);
            ResultSet rs = theStatement.executeQuery();
            OjtReflection student = null;
            
            while(rs.next()){
                student = new OjtReflection(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4));
            }
            return student;
        }catch (SQLException ex) {
            Logger.getLogger(OjtDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }*/
     public static OjtReflection select(int idIn) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        OjtReflection student = null;
        try {
            conn = ConnectionUtils.getConnection();
            System.out.println("Loading for: " + idIn);
            sql = "SELECT * FROM ojtreflection where id = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idIn);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                int id = rs.getInt(1);
                int studentId = rs.getInt("studentId");
                String name = rs.getString("name");
                String reflection = rs.getString("reflection");
                student = new OjtReflection(id, studentId, name,reflection);
                
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return student;
    }
     /**
     * This method will delete the student associated with the student id.
     *
     * @param studentId Identifies the student to be deleted.
     * 
     * @author Divya S
     */
    public static synchronized void delete(int idnum) throws Exception  {
         PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;


        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            sql = "DELETE FROM `ojtreflection` WHERE id=?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idnum);

            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }

    }
/**
     * This method will insert the input student. It will notify the console if
     * an exception is encountered (IE student already exists).
     *
     * @param student -> OjtReflection to insert
     * 
     * @author Divya S
     */
    /*public static synchronized void insert(OjtReflection student) throws Exception {
        Connection conn = null;
        Statement stmt = null;
        try {

            
            conn = ConnectionUtils.getConnection();
            PreparedStatement insertStatement = null;
            String sqlString = "INSERT INTO `ojtreflection`(`id`, `studentId`, `name`, `reflection`)"
                    + "VALUES (?,?,?,?)";
            insertStatement = conn.prepareStatement(sqlString);
            insertStatement.setInt(1, student.getId());
            insertStatement.setInt(2, student.getStudentId());
            insertStatement.setString(3, student.getName());
            insertStatement.setString(4, student.getReflection());

            
            
            insertStatement.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("Could not add student into ojt table");
        }

    }*/
     /**
     * Will update the database based on student provided.
     *
     * @param student-> ojtReflection to update
     *
     * @author Divya S
     */
    /*public static synchronized void update(OjtReflection student) throws Exception {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = ConnectionUtils.getConnection();
            String query = "UPDATE ojtreflection SET "
                    + "studentId='"+student.getStudentId()+"',name='"+
                    student.getName()+"',reflection='"+student.getReflection()
                    +"' WHERE id="+student.getId();
            System.out.println("About to query:" + query);
            stmt.executeUpdate(query);

        } catch (SQLException sqle) {
            System.out.println("Could not add student");
        }
    }*/
    
    
    
    public static synchronized OjtReflection update(OjtReflection student) throws Exception {
//        System.out.println("inserting camper");
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;


        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            //Check to see if camper exists.
            //Note that the default for an Integer is null not 0  !!!
            //When I try to compare the getId() to null it throws an exception.
            
            int studentIdInt;
            try {
                studentIdInt = student.getId();
            } catch (Exception e) {
                studentIdInt = 0;

            }

            if (studentIdInt == 0) {

                sql = "SELECT max(id) from ojtreflection";
                ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                int max = 0;
                while (rs.next()) {
                    max = rs.getInt(1) + 1;
                }

                student.setId(max);

                sql = "INSERT INTO `ojtreflection`(`id`, `studentId`, `name`, `reflection`) "
                        + "VALUES (?,?,?,?)";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, student.getId());
                ps.setInt(2, student.getStudentId());
                ps.setString(3, student.getName());
                ps.setString(4, student.getReflection());

            } else {

                 sql = "UPDATE `ojtreflection` SET `studentId`=?,`name`=?,`reflection`=? WHERE id = ?";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, student.getStudentId());
                ps.setString(2, student.getName());
                ps.setString(3, student.getReflection());
                ps.setInt(4, student.getId());

            }
            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */

            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }
        return student;

    }
}
