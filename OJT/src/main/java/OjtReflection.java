/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import java.util.Scanner;
/**
 *This class will represent a student.
 * @author dswaminathan
 * @since 20190918
 */
public class OjtReflection {
    private static int maxStudentId;
    private int studentId;
    private String firstName;
    private String lastName;
    private String reflectionText;
    
    public OjtReflection()
    {
         this.studentId = ++maxStudentId;
    }
   
    public OjtReflection(boolean getFromUser)
    {
         if (getFromUser) {
            System.out.println("Enter first name");
            this.firstName = FileUtility.getInput().nextLine();

            System.out.println("Enter last name");
            this.lastName = FileUtility.getInput().nextLine();

            System.out.println("Enter relection text");
            this.reflectionText = FileUtility.getInput().nextLine();

        }
         this.studentId = ++maxStudentId;
    }
    public void updateReflection(){
        System.out.println("Enter new reflection");
        Scanner input = new Scanner(System.in);
        this.reflectionText = input.nextLine();
    }
     /**
     * Custom constructor with all info
     *
     * @param registrationId
     * @param firstName
     * @param lastName
     * @param reflectionText
     *
     * @author dswaminathan
     * @since 20190918
     */
    
    public OjtReflection(int studentId, String firstName, String lastName, String reflectionText)
    {
        this.studentId = studentId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.reflectionText = reflectionText;
    }

     public OjtReflection(String[] parts) {
        this(Integer.parseInt(parts[0]), parts[1], parts[2], parts[3]);
        /*
         This makes sure that we capture/set the maximum registration id as we load
         all of the entries from the file.  Then when we add a new camper it will
         use this to set the next registration id.
         */
        if (Integer.parseInt(parts[0]) > maxStudentId) {
            maxStudentId = Integer.parseInt(parts[0]);
        }
    }
    /**
     * @return the studentId
     */
    public int getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the reflectionText
     */
    public String getReflectionText() {
        return reflectionText;
    }

    /**
     * @param reflectionText the reflectionText to set
     */
    public void setReflectionText(String reflectionText) {
        this.reflectionText = reflectionText;
    }
    
    @Override
    public String toString(){
        return "studentId=" +studentId +", firstName=" +firstName + ", lastName=" + lastName + ", relectiontext=" + reflectionText;
    }
    public String getJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
     /**
     * give back a new instance based on the json string passed in.
     *
     * @since 20190918
     * @author dswaminathan
     */
    public static OjtReflection newInstance(String jsonIn) {
        Gson gson = new Gson();
        return gson.fromJson(jsonIn, OjtReflection.class);
    }
     public static int getMaxStudentId() {
        return maxStudentId;
    }

    public static void setMaxStudentId(int maxStudentId){
        OjtReflection.maxStudentId = maxStudentId;
    }

}
